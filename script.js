// VARIABLE GLOBAL
const gameChoiseComputer = document.querySelectorAll('.option-player2 div');
let vs = document.getElementById ('vs')
let info = document.getElementById('info')

// PILIHAN COMPUTER
function getComputerChoise() {
    
    let random = Math.floor(Math.random() * (3 - 0) + 0);
    let x = gameChoiseComputer[random]
    return x  
}


// LOGIKA PERMAINAN
function getResult(computer, player) {
    if ( player == computer) return 'DRAW';

    if ( player == 'rock') return ( computer == 'paper') ? 'PLAYER 1 LOOSE' : 'COMPUTER WIN';

    if ( player == 'paper' ) return ( computer == 'scissors') ? 'PLAYER 1 LOOSE' : 'COMPUTER WIN';

    if ( player == 'scissors' ) return ( computer == 'rock' ) ? 'PLAYER 1 LOOSE' : 'COMPUTER WIN';
}


// PILIHAN PLAYER
const gameChoisePlayer = document.querySelectorAll('.option-player1 div');
    gameChoisePlayer.forEach((choise)=>{
    choise.addEventListener ('click', ()=>{
//TAMPILAN OPSI YANG DIPILIH PLAYER
        const playerChoise = choise.id;
        gameChoisePlayer.forEach (i => i.classList.remove('clicked-change')); choise.classList.add('clicked-change')

//PILIHAN COMPUTER (DISIMPAN DI VARIABLE GLOBAL)      
//TAMPILAN OPSI YANG DIPILIH COMPUTER
        const computerChoise = getComputerChoise();

        gameChoiseComputer.forEach (x => x.classList.remove('clicked-change')); computerChoise.classList.add('clicked-change')

        const gameResult = getResult(computerChoise.id , playerChoise)

//TAMPILAN HASIL PERMAINAN
        info.innerHTML = gameResult;
        const resultInfo = document.getElementById('info').classList.replace('vs-mark' , 'info-result')

        console.log ('computer :' + computerChoise.id)
        console.log ('player :' + playerChoise)
        console.log (gameResult)
    })

})


// ketika diklik tombol refresh VS mark muncul, dan siap adu suit
const btnRefresh = document.querySelector('.refresh');
btnRefresh.addEventListener('click',function () {
    

    // reset tombol option player
    const gamePlayerChoise = document.querySelectorAll('.option-player1 div')
    gamePlayerChoise.forEach(e => e.classList.remove('clicked-change'))

    // reset tombol option computer
    const gameComputerChoise = document.querySelectorAll('.option-player2 div')
    gameComputerChoise.forEach(e => e.classList.remove('clicked-change'))

    const vsMark = document.getElementById('info');
    vsMark.innerHTML = "VS";

    const vsInfo = document.getElementById('info').classList.replace('info-result' , 'vs-mark')
    });

    